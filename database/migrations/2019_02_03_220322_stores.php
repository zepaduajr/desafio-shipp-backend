<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->string('county');
            $table->string('license_number')->primary();
            $table->string('operation_type');
            $table->string('establishment_type');
            $table->string('entity_name');
            $table->string('dba_name');
            $table->integer('street_number');
            $table->string('street_name');
            $table->string('address_line_2');
            $table->string('address_line_3');
            $table->string('city');
            $table->string('state');
            $table->integer('zip_code');
            $table->integer('square_footage');
            $table->double('latitude');
            $table->double('longitude');
            $table->double('cos_lat_rad');
            $table->double('sin_lat_rad');
            $table->double('cos_lon_rad');
            $table->double('sin_lon_rad');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
