<?php

namespace App\Services;


use App\Repositories\StoreRepository;
use App\Http\Resources\StoreResource;
use Illuminate\Support\Collection;

class StoreService
{
    /**
     * @var StoreRepository
     */
    private $repository;

    public function __construct(StoreRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listar($latitude, $longitude)
    {
        $cos_lat_rad = cos($this->deg2rad($latitude));
        $sin_lat_rad = sin($this->deg2rad($latitude));
        $cos_lon_rad = cos($this->deg2rad($longitude));
        $sin_lon_rad = sin($this->deg2rad($longitude));
        
        $stores = $this->repository->getStores($cos_lat_rad, $sin_lat_rad, $cos_lon_rad, $sin_lon_rad);

        $stores = $stores->reject(function ($item) {
            if($this->calcularDistanciaKm($item->distance_acos) > env('DISTANCIA_MAXIMA', 6.5)){
                return $item;
            }
        });
        
        return StoreResource::collection($stores);
    }

    private function calcularDistanciaKm($distancia)
    {
        return (!is_nan(acos($distancia))) ? acos($distancia) * 6387.7 : 0;
    }

    private function deg2rad($deg)
    {
        return ($deg * M_PI / 180.0);
    }
}