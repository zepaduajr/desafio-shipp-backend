<?php

namespace App\Services;


use Illuminate\Support\Collection;
use App\Repositories\LogRequestRepository;
use App\Http\Resources\LogRequestResource;

class LogRequestService
{
    /**
     * @var LogRequestRepository
     */
    private $repository;

    public function __construct(LogRequestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listar()
    {        
        return LogRequestResource::collection($this->repository->getLogRequests());
    }
}