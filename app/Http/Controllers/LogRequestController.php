<?php

namespace App\Http\Controllers;

use App\Services\LogRequestService;
use Illuminate\Http\Request;

class LogRequestController extends Controller
{

    /**
     * @var LogRequestService
     */
    private $LogRequestService;

    public function __construct(LogRequestService $LogRequestService)
    {
        $this->LogRequestService = $LogRequestService;
    }

    public function index()
    {
        return $this->LogRequestService->listar();
    }
}