<?php

namespace App\Http\Controllers;

use App\Services\StoreService;
use Illuminate\Http\Request;

class StoreController extends Controller
{

    /**
     * @var StoreService
     */
    private $storeService;

    public function __construct(StoreService $storeService)
    {
        $this->storeService = $storeService;
    }

    public function index(Request $request)
    {
        $data = $request->all();
        return $this->storeService->listar($data['latitude'], $data['longitude']);
    }
}