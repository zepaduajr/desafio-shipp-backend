<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;
use App\Entities\LogRequest;
use Illuminate\Support\Collection;

class ValidateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => ['required','regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,7})?))$/'],
            'longitude' => ['required','regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,7})?))$/'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $content = json_decode($response->getContent());
        $stores_number = (isset($content->data)) ? (count($content->data)) ?? 0 : 0;
        $log = new LogRequest();
        $log->date_request = date('d/m/Y H:i:s');
        $log->request = Collection::make($request->all())->toJson();
        $log->status_code = $response->status();
        $log->stores_number = $stores_number;
        $log->save();
    }
}
