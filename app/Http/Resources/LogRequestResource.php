<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class LogRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'log_requests_id' => $this->log_requests_id,
            'date_request' => $this->date_request,
            'request' => $this->request,
            'status_code' => $this->status_code,
            'stores_number' => $this->stores_number
        ];
        
    }
}