<?php
/**
 * Created by PhpStorm.
 * User: zepaduajr
 * Date: 03/02/2019
 * Time: 20:33
 */

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'county' => $this->county,
            'license_number' => $this->license_number,
            'operation_type' => $this->operation_type,
            'establishment_type' => $this->establishment_type,
            'entity_name' => $this->entity_name,
            'dba_name' => $this->dba_name,
            'street_number' => $this->street_number,
            'street_name' => $this->street_name,
            'address_line_2' => $this->address_line_2,
            'address_line_3' => $this->address_line_3,
            'city' => $this->city,
            'state' => $this->state,
            'zip_code' => $this->zip_code,
            'square_footage' => $this->square_footage,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'distance' => (!is_nan(acos($this->distance_acos))) ? round(acos($this->distance_acos) * 6387.7,1) : 0, 
        ];
        
    }
}