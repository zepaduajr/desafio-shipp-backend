<?php
/**
 * Created by PhpStorm.
 * User: zepaduajr
 * Date: 03/02/2019
 * Time: 20:36
 */

namespace App\Repositories;


use App\Entities\Store;
use App\Http\Resources\StoreResource;

class StoreRepository
{

    /**
     * @var Store
     */
    private $model;

    public function __construct(Store $model)
    {
        $this->model = $model;
    }

    public function resource()
    {
        return StoreResource::class;
    }

    public function getStores($cos_lat_rad, $sin_lat_rad, $cos_lon_rad, $sin_lon_rad)
    {
        return $this->model
            ->selectRaw('*, (sin_lat_rad * '.$sin_lat_rad.' + cos_lat_rad * '.$cos_lat_rad.' * (sin_lon_rad * '.$sin_lon_rad.'  + cos_lon_rad * '.$cos_lon_rad.')) AS distance_acos ')
            ->where('latitude','<>','')
            ->where('longitude','<>','')
            ->orderBy('distance_acos','desc')
            ->get();
    }
}