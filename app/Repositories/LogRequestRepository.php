<?php

namespace App\Repositories;


use App\Entities\LogRequest;
use App\Http\Resources\LogRequestResource;

class LogRequestRepository
{

    /**
     * @var LogRequest
     */
    private $model;

    public function __construct(LogRequest $model)
    {
        $this->model = $model;
    }

    public function resource()
    {
        return LogRequestResource::class;
    }

    public function getLogRequests()
    {
        return $this->model
            ->paginate();
    }
}