<?php

namespace App\Console\Commands;

use App\Entities\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ImportDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for import csv to sqlite.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Verifica se o arquivo da base de dados ja esta criado.
        if (!file_exists(database_path('database.sqlite'))){
            fopen(database_path('database.sqlite'), "wb");
        }
        //Verifica se o arquivo stores.csv ja foi carregado
        if (!file_exists(storage_path('app/public/stores.csv'))){
            copy(database_path('stores.csv'), storage_path('app/public/stores.csv'));
        }
        //Migrate das tabelas necessárias
        Artisan::call('migrate:fresh');

        $this->info('Iniciando a importação dos dados.');

        $qtd = 0;
        //Transforma dados do csv em array
        $data = $this->csvToArray('app/public/stores.csv');
        foreach ($data as $dt){
            $dt = array_map("trim", $dt);
            $qtd++;
            if(isset($dt['County'])){

                $location = str_replace('"','',$dt['Location']);
                $location = str_replace("'","",$location);

                $latitude = explode('latitude: ', $location);
                $latitude = (isset($latitude[1])) ? preg_replace("/[^0-9\.]/", '', $latitude[1]) : '';

                $longitude = explode('longitude: ', $location);
                $longitude = (isset($longitude[1])) ? explode(',', $longitude[1])[0] : '';
                $cos_lat_rad = '';
                $sin_lat_rad = '';
                $cos_lon_rad = '';
                $sin_lon_rad = '';
                if($latitude != '' && $longitude != ''){
                    $cos_lat_rad = cos($this->deg2rad($latitude));
                    $sin_lat_rad = sin($this->deg2rad($latitude));
                    $cos_lon_rad = cos($this->deg2rad($longitude));
                    $sin_lon_rad = sin($this->deg2rad($longitude));
                }
                //Insere as informações contendo o seno e consseno de latitude e longitude para serem usados no calculo da distancia.
                Store::create([
                    'county' => $dt['County'],
                    'license_number' => $dt['License Number'],
                    'operation_type' => $dt['Operation Type'],
                    'establishment_type' => $dt['Establishment Type'],
                    'entity_name' => $dt['Entity Name'],
                    'dba_name' => $dt['DBA Name'],
                    'street_number' => $dt['Street Number'],
                    'street_name' => $dt['Street Name'],
                    'address_line_2' => $dt['Address Line 2'],
                    'address_line_3' => $dt['Address Line 3'],
                    'city' => $dt['City'],
                    'state' => $dt['State'],
                    'zip_code' => $dt['Zip Code'],
                    'square_footage' => $dt['Square Footage'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'cos_lat_rad' => $cos_lat_rad,
                    'sin_lat_rad' => $sin_lat_rad,
                    'cos_lon_rad' => $cos_lon_rad,
                    'sin_lon_rad' => $sin_lon_rad,
                ]);
            }
        }

        $this->info($qtd.' Lojas importadas.');
    }

    private function csvToArray($file): array
    {
        $header = false;
        $data = array();
        if (($handle = fopen(storage_path($file), 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, ",")) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    private function deg2rad($deg)
    {
        return ($deg * M_PI / 180.0);
    }
}
