<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


/**
 * Class LogRequest
 * @package namespace App\Entities;
 */

class LogRequest extends Model
{
    protected $table = 'log_requests';
    protected $primaryKey = 'log_requests_id';

    public $timestamps = false;

    protected $fillable = [
        'date_request',
        'request',
        'status_code',
        'stores_number',
    ];

    
}


