<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Store
 * @package namespace App\Entities;
 */

class Store extends Model
{
    protected $table = 'stores';

    public $timestamps = false;

    protected $fillable = [
        'county',
        'license_number',
        'operation_type',
        'establishment_type',
        'entity_name',
        'dba_name',
        'street_number',
        'street_name',
        'address_line_2',
        'address_line_3',
        'city',
        'state',
        'zip_code',
        'square_footage',
        'latitude',
        'longitude',
        'cos_lat_rad',
        'sin_lat_rad',
        'cos_lon_rad',
        'sin_lon_rad',
    ];

    
}


