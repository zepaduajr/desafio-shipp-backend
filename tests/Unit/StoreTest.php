<?php

namespace Tests\Unit;

use Tests\TestCase;

class StoreTest extends TestCase
{
    /**
     * Teste de requisição sem enviar parâmetro.
     *
     * @return void
     */
    public function testSemParametro()
    {
        $this->get('api/v1/store')->assertStatus(400);
    }

    /**
     * Teste de requisição informando somente longitude
     *
     * @return void
     */
    public function testSemLatitude()
    {
        $url = '?longitude=-73.691984';

        $this->get('api/v1/store'.$url)->assertStatus(400);
    }
    /**
     * Teste de requisição informando somente latitude
     *
     * @return void
     */
    public function testSemLongitude()
    {
        $url = '?latitude=42.746003';

        $this->get('api/v1/store'.$url)->assertStatus(400);
    }
    /**
     * Teste de requisição informando dados corretos
     *
     * @return void
     */
    public function testCorreto()
    {
        $url = '?latitude=42.746003';
        $url .= '&longitude=-73.691984';

        $this->get('api/v1/store'.$url)->assertStatus(200);
    }
    /**
     * Teste de requisição informando latitude inválida.
     *
     * @return void
     */
    public function testLatitudeInvalido()
    {
        $url = '?latitude=aaa';
        $url .= '&longitude=-73.691984';

        $this->get('api/v1/store'.$url)->assertStatus(400);
    }
    /**
     * Teste de requisição informando longitude inválida.
     *
     * @return void
     */
    public function testLongitudeInvalido()
    {
        $url = '?latitude=42.746003';
        $url .= '&longitude=aaa';

        $this->get('api/v1/store'.$url)->assertStatus(400);
    }
    /**
     * Teste de requisição informando latitude e longitude corretos porém distante de qualquer ponto.
     *
     * @return void
     */
    public function testLatitudeLongitudeLonges()
    {
        $url = '?latitude=-20.317655';
        $url .= '&longitude=-40.302299';
        $response = $this->get('api/v1/store'.$url);
        $response = json_decode($response->getContent());
        $this->get('api/v1/store'.$url)->assertStatus(200);
        $this->assertEmpty($response->data);
    }
}
